const expect = require('expect.js')
const app = require('../src/app');

const port = app.get('port') || 3030;

describe('Feathers application tests', () => {
    before(function(done) {
        this.server = app.listen(port);
        this.server.once('listening', () => done());
    });
    after(function(done) {
        this.server.close(done);
    });

    describe('Ikarus tests', function() {
      this.timeout(5000)
      let schwalbe = {}
      let storch = {}
      it('lets you log in', async () => {
        schwalbe = await app.service('/authentication').create({ strategy: 'local', name: 'Schwalbe', password: 'secret' }, { provider: 'rest' })
        storch = await app.service('/authentication').create({ strategy: 'local', name: 'Storch', password: 'secret' }, { provider: 'rest' })

        expect(schwalbe.user.id).to.be.a('number')
        expect(storch.user.id).to.be.a('number')
      })

      it('applies rules', async () => {
        expect(schwalbe.user.password).to.be(undefined)
        expect(storch.user.password).to.be(undefined)
      })

      it('assigns user_id to new tasks', async () => {
        schwalbe.task = await app.service('/tasks').create({ title: 'Test', user_id: 'some-bullshit-value' }, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: schwalbe.accessToken } })
        storch.task = await app.service('/tasks').create({ title: 'Test', user_id: 'some-bullshit-value' }, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: storch.accessToken } })

        expect(storch.task.user_id).to.be(storch.user.id)
        expect(schwalbe.task.user_id).to.be(schwalbe.user.id)
      })

      it('only shows own tasks', async () => {
        const storch_tasks = (await app.service('/tasks').find({ query: { $limit: 50 }, provider: 'rest', authentication: { strategy: 'jwt', accessToken: storch.accessToken } })).data
        expect(storch_tasks.map(task => task.user_id)).not.to.contain(schwalbe.user.id)

        let result = null
        try {
          result = await app.service('/tasks').get(storch_tasks[0].id, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: schwalbe.accessToken } })
        } catch(error) {}
        if(result != null) expect().fail('You shouldnt be able to get a foreign task')
      })

      it('only lets users change their own tasks', async () => {
        let result = null
        try {
          result = await app.service('/tasks').patch(storch.task.id, { title: 'patched!!' }, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: schwalbe.accessToken } })
        } catch(error) {}

        if(result != null) expect().fail('You shouldnt be able to change a foreign task')
      })

      it('only lets users delete their own tasks', async () => {
        let response = null
        try {
          response = await app.service('/tasks').remove(schwalbe.task.id, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: storch.accessToken } })
        } catch(error) {}

        if(response != null) expect().fail('You shouldnt be able to remove a foreign task')
      })

      it('prevents users from changing user_id', async () => {
        const task = await app.service('/tasks').patch(storch.task.id, { user_id: schwalbe.user.id }, { provider: 'rest', authentication: { strategy: 'jwt', accessToken: storch.accessToken } })

        expect(task.user_id).to.be(storch.user.id)
      })

      it('hashes user passwords', async () => {
        const user = await app.service('/users').create({ name: 'testuser', email: 'testuser@lol.de', password: 'secret' })
        expect(user.password).not.to.be('secret')

        await app.service('/users').remove(user.id)
      })
    })
});
