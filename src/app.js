const app = require('@ikarus/app')

module.exports = app({
  bootstrap: require('./bootstrap'),
  endpoints: require('./endpoints')
})
