const { allocator } = require('@ikarus/channels')
const objection = require('@ikarus/objection').initializer

module.exports = [
  objection,
  allocator(),
]
