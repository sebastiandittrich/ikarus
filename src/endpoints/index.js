module.exports = {
  '/tasks': require('./tasks'),
  '/users': require('./users'),
  '/authentication': require('./authentication')
}
