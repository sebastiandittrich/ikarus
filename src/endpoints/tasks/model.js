const { Model } = require('@ikarus/objection')

module.exports = class Task extends Model {
  static get jsonSchema() {
    return {
        type: 'object',
        required: ['title', 'user_id'],

        properties: {
            title: { type: 'string' },
            user_id: { type: 'integer' },
        }
    };
  }
}
