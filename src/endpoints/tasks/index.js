const { Endpoint } = require('@ikarus/objection')
const { restrictToOwner, associateCurrentUser, needsAuthentication } = require('@ikarus/rules')

module.exports = Endpoint({
  model: require('./model'),
  rules: [
    needsAuthentication('jwt'),
    restrictToOwner({ foreign_key: 'user_id' }),
    associateCurrentUser({ foreign_key:'user_id' }),
  ]
})
