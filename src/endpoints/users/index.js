const { Endpoint } = require('@ikarus/objection')
const { protect, hash } = require('@ikarus/rules')

module.exports = Endpoint({
  model: require('./model'),
  rules: [
    protect('password'),
    hash('password')
  ]
})
