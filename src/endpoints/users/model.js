const { Model } = require('@ikarus/objection')

module.exports = class User extends Model {
  static get jsonSchema() {
      return {
          type: 'object',
          required: ['email', 'password'],

          properties: {
              name: { type: 'string' },
              email: { type: 'string' },
              password: { type: 'string' }
          }
      };
  }
}
