const { JWTStrategy, LocalStrategy, Endpoint } = require('@ikarus/authentication')

module.exports = Endpoint({
  endpoint: '/users',
  strategies: { jwt: JWTStrategy, local: LocalStrategy },
})
